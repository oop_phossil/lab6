package com.phossil.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank phossil = new BookBank("Phossil", 100.0);
        phossil.print();
        phossil.deposit(50);
        phossil.print();
        phossil.withdraw(50);
        phossil.print();

        BookBank sickadui = new BookBank("Sickadui", 1);
        sickadui.deposit(1000000);
        sickadui.withdraw(10000000);
        sickadui.print();
        
        BookBank reywon = new BookBank("Reywon", 10);
        reywon.deposit(10000000);
        reywon.withdraw(1000000);
        reywon.print();
    }
}
